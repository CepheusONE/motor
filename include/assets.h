#ifndef ASSETS_H
#define ASSETS_H

#include "filesystem.h"
#include "window.h"
#include "glob.h"
#include "parser.h"
#include <SDL.h>
#include <SDL_image.h>
#include <string>

class Asset : public virtual Filesystem, public YML
{
private:
	int AssetID;
	std::string ImageManip;
	const std::string File;

public:
	SDL_Texture *AssetOpt;
	SDL_Surface *TempSurface;
	SDL_Surface *TempBackground;
	SDL_Rect SrcR;
	SDL_Rect DestR;
	SDL_RWops *RW;
	int Velocity;
	int AssetHeight;
	int AssetWidth;
	int AssetXOfst;
	int AssetYOfst;
	int ClipHeight;
	int ClipWidth;

public:
	Asset();
	~Asset();
	void DestTexture();
	void DrawAsset(std::string ImageManip);
	void DestAsset();
	int LoadAsset(int AssetID, const std::string File);
	int SetRects(int AssetWidth, int AssetHeight, int AssetXOfst, int AssetYOfst);
	int AssetStretch(int AssetWidth, int AssetHeight);
	int SetClip(int ClipWidth, int ClipHeight);
	int DestSurface();
};

#endif

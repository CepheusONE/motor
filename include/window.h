#ifndef WINDOW_H
#define WINDOW_H

#include <SDL.h>
#include <SDL_image.h>
#include <string>
#include <iostream>
#include <fstream>

SDL_Renderer *GetRenderer();
const std::string WindowName;
void NumDisplay();
void DestRenderer();
void DestWindow();
int CreateWindow(int w_width, int w_height, const std::string WindowName);
void SetWindowIcon(const std::string Icon);

#endif

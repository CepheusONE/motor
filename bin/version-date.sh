#!/bin/bash

DATE=`date +%F`
SHA=`git rev-parse HEAD`

echo "#ifndef VERSION_H" > include/version.h
echo "#define VERSION_H" >> include/version.h
echo "extern const char *build_date;" >> include/version.h
echo "extern const char *build_git_sha;" >> include/version.h
echo "#endif" >> include/version.h

echo "#include \"version.h\"" > src/version.cpp
echo "const char *build_date = \"$DATE\";" >> src/version.cpp
echo "const char *build_git_sha = \"$SHA\";" >> src/version.cpp

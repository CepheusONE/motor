#include "glob.h"
#include <SDL.h>
#include <SDL_image.h>
#include <iostream>
#include <fstream>

// Global SDL2 error print
void SDLErrorPrint()
{
	std::cout << SDL_GetError() << "\n";
}

// Global SDL2_image error print
void IMGErrorPrint()
{
	std::cout << IMG_GetError() << "\n";
}

// Writes stdout to log.txt in the binary's root directory.
// Replaces log.txt if exists.
void WriteToLog()
{
	std::freopen("log.txt", "w", stdout);
}

bool UserDebug(int SetUserDebug)
{
	if (SetUserDebug == 1) {
		std::cout << "User debug is set to true\n";
		return true;
	} else if (SetUserDebug == 0) {
		std::cout << "User debug is set to false\n";
		return false;
	}
	return false;
}

bool GlobalDebug(int SetGlobalDebug)
{
	if (SetGlobalDebug == 1) {
		std::cout << "Global debug is set to true\n";
		UserDebug(1);
		return true;
	} else if (SetGlobalDebug == 0) {
		std::cout << "Global debug is set to false\n";
		return false;
	}
	return false;
}

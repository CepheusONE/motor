#include "text.h"
#include <SDL_ttf.h>
#include <iostream>

Text::Text()
{
}

Text::~Text()
{
	DestFont();
}

int Text::SetFont(int FontHeight, const std::string Font)
{
	T_Font = TTF_OpenFont(Font.c_str(), FontHeight);
	return 0;
}

int Text::LoadText(const std::string PrintText, Uint8 fR, Uint8 fG, Uint8 fB, Uint8 fA)
{
	SDL_Color FontColor = { fR, fG, fB, fA };
	FontSurface = TTF_RenderText_Solid(T_Font, PrintText.c_str(), FontColor);
	FontOpt = SDL_CreateTextureFromSurface(GetRenderer(), FontSurface);

	if (FontSurface != NULL) {
		SDL_FreeSurface(FontSurface);
	}
	if (T_Font != NULL) {
		TTF_CloseFont(T_Font);
	}
	return 0;
}

void Text::DrawText()
{
	SDL_RenderCopy(GetRenderer(), FontOpt, NULL, NULL);
	SDL_RenderPresent(GetRenderer());
}

void Text::DestFont()
{
	if (FontOpt != NULL) {
		SDL_DestroyTexture(FontOpt);
		std::cout << "Font " << Font << " destroyed\n";
	} else if (FontOpt == NULL) {
		std::cout << "No font to destroy\n";
	}
}

#include "parser.h"
#include <iostream>
#include <fstream>

YML::YML()
{
}

YML::~YML()
{
}

int YML::SetParseFile(const std::string YMLFile)
{
	FILE *oFile = fopen(YMLFile.c_str(), "r");
	if (oFile != NULL) {
		yaml_parser_initialize(&Parser);
		yaml_parser_set_input_file(&Parser, oFile);
		if (!yaml_parser_load(&Parser, &Doc)) {
			printf("Parser done broke brah\n");
		}
		Map = yaml_document_get_root_node(&Doc);
		assert(Map->type == YAML_MAPPING_NODE);
		for (Pair = Map->data.mapping.pairs.start; Pair < Map->data.mapping.pairs.top; ++Pair) {
			Key = yaml_document_get_node(&Doc, Pair->key);
			Val = yaml_document_get_node(&Doc, Pair->value);
			assert(Key->type == YAML_SCALAR_NODE);

			if (strcmp((const char *)Key->data.scalar.value, "Values") == 0) {
				assert(Val->type == YAML_SEQUENCE_NODE);
				for (Val_Item = Val->data.sequence.items.start;
				     Val_Item < Val->data.sequence.items.top;
				     ++Val_Item) {
					Node = yaml_document_get_node(&Doc, *Val_Item);
				}
			}
		}

		yaml_document_delete(&Doc);
		yaml_parser_delete(&Parser);
	} else {
		printf("Parser: Document %s does not exist\n", YMLFile.c_str());
	}
	return 0;
}
